
### Struktura URL

Rozdělte následující url na jednotlivé prvky a pojmenujte je.

```
https://test.r2b2.cz/a1/b2?article=12&widgets[]=a&widgets[]=b#foo
```

### Priority selektorů

Vysvětlete jakou prioritu má jaký typ selektoru v CSS. Uvedtě na Vámi vybraném příkladu.

### Kompatibilita s prohlížeči

Jak zjistíte zda je Vaše CSS kompatibilní s určitými prohlížeči?

### Odpovědi:
1. Priority selektorů – Obecně platí, že element si vezme např. barvu písma ze specifického selektoru na rozdíl od pravidla, které mu dává rodič. Takže pokud mám např. body id=rodic a id rodic má mít v CSS barvu písma modrou, ale my nastavíme h1 na zelenou, tak nadpis bude zeleně. Největší prioritu mají id selektory, potom class selektory a následně type selektory. Zároveň platí, že pokud máme style uvnitř elementu, tak ten vždy přepíše cokoliv, co je napojeno na externí CSS stylesheet.

2. Kompatibilita s prohlížeči – Můžu kód přímo otestovat na jednotlivých prohlížečích. Dále se dá použít stránka Can I use, kde můžu najít kompatibilitu příkazů s prohlížeči. 

3. Struktura URL
- http:// je protokol/schéma
- test.r2b2.cz je doména
- /a1/b2 je cesta k umístění na serveru nebo na disku
- b2 jméno souboru
- ? za otazníkem začíná tzv. Query string – je to parametr nebo program, na který se ptáme
- # je id kotvy, která odkazuje na další místo v dokumentu
