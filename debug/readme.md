## 300x600 Banner debug

Úkolem je opravit vzhled reklamního banneru na stránce, kde chybějí některé povinné prvky a je rozbito formátování (viz. úkoly).

Žádné externí knihovny nesmí být použity. Mohou být použity pouze externí open source fonty (např. google fonts).

Kód musí být kompatibilní se staršími prohlížeči and nesmí rozbít styly a jiné prvky a styly na stránce.

Můžete psát kód mezi komentáře: `<!--    Edit only between these comments    -->`.

Úkoly:

- [x] vycenterujte obsahu banneru jak **horizontálně**, tak i **vertikálně**
- [x] opravte reklamní popisek "Reklama", tak aby byl viditelný v banneru
- [x] zjistěte proč se banner nezobrazuje na mobilním zařízení.
- [x] (volitelné) nastylujete banner tak, aby více zaujmul uživatele stránky.
- [x] (volitelné) upravte HTML a CSS tak, aby se banner zobrazil i na mobilních zařízení.

### Poznámky k debugu
Banner se nezobrazoval na mobilních zařízeních, protože v kódu je napsáno, že \@media \(max-width: 800px) \{aside – display: none}. Příkaz tedy říkal, že se banner nemá zobrazit potom, co se rozlišení dostane pod 800px. Příkaz sem zakomponoval, takže by se měl nyní zobrazovat na mobilních zařízeních. Dále jsem lehce upravil obsah, aby byl trošku zajímavější.
